"use strict";
exports.__esModule = true;
var ValidationService;
(function (ValidationService) {
    var ValidationAPIService = /** @class */ (function () {
        function ValidationAPIService() {
            this.validationCreated = [];
        }
        ValidationAPIService.prototype.createValidation = function (validationData) {
            if (Math.random() <= 0.1) {
                return Promise.reject(new Error('Bad Gateway'));
            }
            else {
                this.validationCreated.push(validationData);
                return Promise.resolve(validationData);
            }
        };
        return ValidationAPIService;
    }());
    ValidationService.ValidationAPIService = ValidationAPIService;
})(ValidationService || (ValidationService = {}));
function generateData(dataCount) {
    if (dataCount === void 0) { dataCount = 10000; }
    var inputData = [];
    var NB_EVALUATED_USERS = 5; // limit to 5 evaluated users
    for (var j = 0; j < NB_EVALUATED_USERS; j++) {
        var evaluatedUser = 'U' + j;
        var evaluations = [];
        for (var i = 0; i < dataCount / NB_EVALUATED_USERS; i++) {
            evaluations.push({
                evaluator: 'U1' + i,
                skill: 'SKI' + i,
                comment: 'comment ' + i
            });
        }
        // an inputData contains the evaluatedUser and a list of evaluations
        // to create validation from evaluations : evaluatedUser <=> validation.user, evaluations[*].evaluator <=> validation.author, evaluations[*].skill <=> validation.skill, evaluations[*].comment <=> validaton.comment
        inputData.push({
            evaluatedUser: evaluatedUser,
            evaluations: evaluations
        });
    }
    return inputData;
}
function execBulkValidationCreator() {
    var inputData = generateData();
    var validationService = new ValidationService.ValidationAPIService();
    for (var _i = 0, inputData_1 = inputData; _i < inputData_1.length; _i++) {
        var input = inputData_1[_i];
        // TODO: create Validation Data from Assessment Data
        // TODO: secure this API call to create validation
        // validationService.createValidation(convertedInput)
    }
}
execBulkValidationCreator();
