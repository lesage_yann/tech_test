import * as _ from 'lodash'

namespace ValidationService {

    /**
     * ValidationData is a way to validate a skill that an user has used in his work.
     */
    interface IValidationData {
        user: string // user receiving validation
        author: string // user giving validation
        skill: string // validation related skill
        comment: string // validation's comment
    }
    
    export class ValidationAPIService {
        validationCreated: IValidationData[];
    
        constructor () {
            this.validationCreated = []
        }
    
        createValidation (validationData: IValidationData) {
            if (Math.random() <= 0.1) {
                return Promise.reject(new Error('Bad Gateway'))
            } else {
                this.validationCreated.push(validationData)
                return Promise.resolve(validationData)
            }
        }
    }

}

namespace AssessmentService {

    interface IAssessmentRowData {
        evaluator: string // user who evaluated the skill
        skill: string
        comment: string
    }
    
    /**
     * AssessmentData represents all the assessment (evaluations) made by colleagues for a particular user
     */
    export interface IAssessmentData {
        evaluatedUser: string
        evaluations: IAssessmentRowData[]
    }
}

function generateData (dataCount: number = 10000) {
    let inputData: AssessmentService.IAssessmentData[] = []
    const NB_EVALUATED_USERS = 5 // limit to 5 evaluated users
    for (let j = 0; j < NB_EVALUATED_USERS; j ++) {
        let evaluatedUser = 'U' + j
        let evaluations = []
        for (let i = 0; i < dataCount / NB_EVALUATED_USERS; i++) {
            evaluations.push({
                evaluator: 'U1' + i,
                skill: 'SKI' + i,
                comment: 'comment ' + i
            })
        }
        // an inputData contains the evaluatedUser and a list of evaluations
        // to create validation from evaluations : evaluatedUser <=> validation.user, evaluations[*].evaluator <=> validation.author, evaluations[*].skill <=> validation.skill, evaluations[*].comment <=> validaton.comment
        inputData.push({
            evaluatedUser,
            evaluations
        })
    }
    return inputData
}

function execBulkValidationCreator () {
    let inputData = generateData()
    let validationService = new ValidationService.ValidationAPIService()
    for (let input of inputData) {
        // TODO: create Validation Data from Assessment Data
        // TODO: secure this API call to create validation
        // validationService.createValidation(convertedInput)
    }
}

execBulkValidationCreator()